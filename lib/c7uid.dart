library c7uid;

final String _encodeChars = "0123456789ABCDEFGHJKMNPQRSTVWXYZ";

final Map<String, int> _decodeMap = const {
  '0': 0,  '1': 1,  '2': 2,  '3': 3,  '4': 4,  '5': 5,  '6': 6, '7': 7,
  '8': 8,  '9': 9,  'A': 10,  'B': 11,  'C': 12,  'D': 13,  'E': 14,  'F': 15,
  'G': 16,  'H': 17,  'J': 18,  'K': 19,  'M': 20,  'N': 21,  'P': 22,  'Q': 23,
  'R': 24, 'S': 25, 'T': 26, 'V': 27,  'W': 28,  'X': 29,  'Y': 30,  'Z': 31
};

class Uid {
  int value;

  Uid.fromInt(this.value);

  Uid.fromString(String string) {
    this.value = Uid.decodeString(string);
  }

  static String sanitizeHumanString(String string) {
    string = string.toUpperCase();
    string.replaceAll('O', '0');
    string.replaceAll('I', '1');
    string.replaceAll('L', '1');
    string.replaceAll('U', 'V');
    return string;
  }

  static int decodeString(String string) {
    int value = 0;
    string.split('').forEach((String char){
      if (!_decodeMap.containsKey(char)) {
        throw new Exception('unknow character: ' + char);
      }
      value = (value << 5) + _decodeMap[char];
    });
    //unmangle
    value = (value & 1 > 0) ? value ^ 0xA5A : value ^ 0x5A6;
    //remove checksum:
    int checksum = (value >> 12) & 3;
    value = ((value >> 14) << 12) + (value & 0xFFF);
    if (checksum != value % 3 + 1){
      throw new Exception('wrong checksum');
    }
    return value;
  }

  static String encodeInt(int value) {
    //add checksum
    value = ((((value >> 12) << 2) + (value % 3 + 1)) << 12) + (value & 0xFFF);
    //mangle
    value = (value & 1 > 0) ? value ^ 0xA5A : value ^ 0x5A6;
    String ret = "";
    while (value > 0) {
      ret = _encodeChars[value & 31] + ret;
      value >>= 5;
    }
    return ret;
  }

  int toInt() {
     return this.value;
  }

  String toString() {
     return Uid.encodeInt(this.value);
  }
}
