library c7uid.test;

import 'package:test/test.dart';
import 'package:c7uid/c7uid.dart';


void main() {

    test('integrity', () {
      for (int i=0; i<1000; i++) {
        expect(Uid.decodeString(Uid.encodeInt(i)), equals(i));
        //var uid = new Uid.fromInt(i);
        //print("$i => $uid");
      }
      //large number
      int i = 12345678901234567890;
      expect(Uid.decodeString(Uid.encodeInt(i)), equals(i));
      //var uid = new Uid.fromInt(i);
      //print("$i => $uid");
    });

    test('values', () {
      expect(Uid.encodeInt(0), equals('5D6'));
      expect(Uid.encodeInt(1), equals('AJV'));
      expect(Uid.encodeInt(2), equals('DD4'));
      expect(Uid.encodeInt(3), equals('6JS'));
      expect(Uid.encodeInt(4), equals('9D2'));
      expect(Uid.encodeInt(5), equals('EJZ'));
      expect(Uid.encodeInt(10), equals('9DC'));
      expect(Uid.encodeInt(100), equals('9E2'));
      expect(Uid.encodeInt(1000), equals('9JE'));
    });

    test('benchmark', () {
      for (int i=0; i<100000; i++) {
        expect(Uid.decodeString(Uid.encodeInt(i)), equals(i));
      }
    });
}
